﻿using Microsoft.Extensions.Configuration;
using System;

namespace SOLID_principles
{
   public class Game : IGame, IInteraction
   {
      private readonly int _min;
      private readonly int _max;
      private readonly int _attempts;
      private readonly IInput _input;
      private readonly IGenerator _generator;
      private readonly IConsoleOutput _output;

      public Game(IConfiguration config, IInput input, IGenerator generator, IConsoleOutput output)
      {
         _min = config.GetValue<int>("MinValue");
         _max = config.GetValue<int>("MaxValue");
         _attempts = config.GetValue<int>("Attempts");
         _input = input;
         _generator = generator;
         _output = output;
      }
      public void Options()
      {
         _output.Display(string.Concat(
                                          "Guess the number!\n"
                                         , $"The random number from {_min} to {_max} will be selected.\n"
                                         , $"You have to guess in {_attempts} attempts. Each try new number will be selected.\n"
                                         , "Want to give it a try? [y/n]"
                                      ));

         while (true)
         {
            string answer = Console.ReadLine();

            switch (answer.ToLower())
            {
               case "y":
                  Exec();
                  _output.Display("\nDo you want to try again?");
                  break;
               case "n":
                  Environment.Exit(-1);
                  break;
               default:
                  _output.Display("Invalid input. Try again...");
                  break;
            }
         }
      }
      public void Exec()
      {
         int count = 0;
         int reference = _generator.Generate(_min, _max);
         _output.Display("\nNumber has been selected.");

         do
         {
            count++;

            int guess = _input.ReceiveNumber();

             if (_input.ProcessNumber(reference, guess))
                return;
         } while (count < _attempts);
      }
   }
}
