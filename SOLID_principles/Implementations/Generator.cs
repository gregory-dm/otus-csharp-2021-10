﻿using System;

namespace SOLID_principles
{
   public class Generator : IGenerator
   {
      public int Generate(int min, int max)
      {
         Random random = new Random();
         return random.Next(min, max);
      }
   }
}
