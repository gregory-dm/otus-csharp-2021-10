﻿using System;

namespace SOLID_principles
{
   public class Input : IInput
   {
      private readonly IConsoleOutput _output;

      public Input(IConsoleOutput output)
      {
         _output = output;
      }

      public int ReceiveNumber()
      {
         _output.Display("Your guess:");
         int number;

         while (true)
         {
            if (Int32.TryParse(Console.ReadLine(), out number))
               return number;
            else
               _output.Display("Invalid input. Try again...");
         }
      }

      public bool ProcessNumber(int reference, int input)
      {
         if (input == reference)
         {
            _output.Display("You guessed it!");
            return true;
         }
         else if (input > reference)
         {
            _output.Display("No, your number is bigger! Take lower:");
            return false;
         }
         else
         {
            _output.Display("No, your number is lower! Take bigger:");
            return false;
         }
      }
   }
}
