﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID_principles
{
   public class ConsoleOutputUTF8 : ConsoleOutput
   {
      public override void Display(string text)
      {
         Console.OutputEncoding = Encoding.UTF8;
         base.Display(text);
      }
   }
}
