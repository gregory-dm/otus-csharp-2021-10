﻿using System;

namespace SOLID_principles
{
   public class ConsoleOutput : IConsoleOutput
   {
      public virtual void Display(string text)
      {
         Console.WriteLine(text);
      }
   }
}
