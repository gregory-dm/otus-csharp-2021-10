﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.IO;

namespace SOLID_principles
{
   class Program
   {
      static void Main(string[] args)
      {
         var builder = new ConfigurationBuilder();
         BuildConfig(builder);

         var host = Host.CreateDefaultBuilder()
            .ConfigureServices((context, services) =>
            {
               services.AddTransient<IConsoleOutput, ConsoleOutputUTF8>();
               services.AddTransient<IGenerator, Generator>();
               services.AddTransient<IInput, Input>();
               services.AddTransient<IGame, Game>();
            })
            .Build();

         var service = ActivatorUtilities.CreateInstance<Game>(host.Services);

         service.Options();
      }

      static void BuildConfig(IConfigurationBuilder builder)
      {
         builder.SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
      }
   }
}
