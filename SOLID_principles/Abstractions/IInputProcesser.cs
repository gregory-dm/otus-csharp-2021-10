﻿namespace SOLID_principles
{
   public interface IInputProcesser
   {
      bool ProcessNumber(int reference, int input);
   }
}