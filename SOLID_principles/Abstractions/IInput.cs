﻿
namespace SOLID_principles
{
   public interface IInput : IInputReceiver, IInputProcesser
   {
   }
}
