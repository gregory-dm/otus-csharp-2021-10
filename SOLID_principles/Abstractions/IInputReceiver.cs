﻿namespace SOLID_principles
{
   public interface IInputReceiver
   {
      int ReceiveNumber();
   }
}