﻿namespace SOLID_principles
{
   public interface IGenerator
   {
      int Generate(int min, int max);
   }
}