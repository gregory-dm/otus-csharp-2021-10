﻿
namespace SOLID_principles
{
   public interface IConsoleOutput
   {
      void Display(string text);
   }
}
