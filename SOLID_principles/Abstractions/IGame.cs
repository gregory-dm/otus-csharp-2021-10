﻿
namespace SOLID_principles
{
   public interface IGame
   {
      void Exec();
   }
}
