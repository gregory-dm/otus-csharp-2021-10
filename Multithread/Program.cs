﻿using System;

namespace Multithread
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Method | Array length | Sum | Time, ms");
            Console.WriteLine("--- | --- | --- | ---");

            int[] array = new int[] { 10000, 1000000, 10000000 };
            for (int i = 0; i < array.Length; i++)
            {
                SumArray sum = new SumArray(array[i]);
                sum.Sum();
                sum.SumMultithread();
                sum.SumPLINQ();
            }
        }
    }
}
