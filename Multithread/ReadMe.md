Windows 10 x64, AMD Ryzen 5 3500U Cores: 4 Logical processors: 8, 16gb RAM

Method | Array length | Sum | Time, ms
--- | --- | --- | ---
Sum | 10000 | 493065 | 0.0379
SumMultithread | 10000 | 493065 | 4.7727
SumPLINQ | 10000 | 493065 | 27.3238
Sum | 1000000 | 49509902 | 7.116
SumMultithread | 1000000 | 49509902 | 21.3428
SumPLINQ | 1000000 | 49509902 | 2.4097
Sum | 10000000 | 494968075 | 28.4045
SumMultithread | 10000000 | 494968075 | 85.8293
SumPLINQ | 10000000 | 494968075 | 15.0402