using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace Multithread
{
    public class SumArray
    {
        private int[] _array;
        private long _sum;
        private readonly int _threadCount = Environment.ProcessorCount;
        private Stopwatch _timer = new Stopwatch();
        private string _methodName;

        public SumArray(int length)
        {
            _array = new int[length];
            Random rand = new Random();

            for (int i = 0; i < _array.Length; i++)
                _array[i] = rand.Next(0, 100);
        }

        public void Sum()
        {
            _sum = 0;
            _timer.Restart();
            for (int i = 0; i < _array.Length; i++)
                _sum += _array[i];
            _timer.Stop();
            _methodName = MethodBase.GetCurrentMethod().Name;
            ShowMessage();
        }

        public void SumMultithread()
        {
            _sum = 0;
            _timer.Restart();
            IEnumerable<int[]> threadArrays = _array.Chunk(_array.Length/_threadCount + 1);
            List<Thread> threads = new List<Thread>();

            foreach(var array in threadArrays)
            {
                Thread thread = new Thread(() => Interlocked.Add(ref _sum, array.Sum()));
                thread.Start();
                threads.Add(thread);
            }

            foreach(var thread in threads)
                thread.Join();

            _timer.Stop();
            _methodName = MethodBase.GetCurrentMethod().Name;
            ShowMessage();
        }

        public void SumPLINQ()
        {
            _sum = 0;
            _timer.Restart();
            _sum = _array.AsParallel().WithDegreeOfParallelism(_threadCount).Sum();
            _timer.Stop();
            _methodName = MethodBase.GetCurrentMethod().Name;
            ShowMessage();
        }

        public void ShowMessage()
        {
            Console.WriteLine($"{_methodName} | {_array.Length} | {_sum} | {_timer.Elapsed.TotalMilliseconds}");
        }
    }
}