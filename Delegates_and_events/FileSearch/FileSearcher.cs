using System;
using System.IO;

namespace Delegates_and_events.FileSearch
{
   public class FileSearcher
   {
      public event EventHandler<FileArgs> FileFound;
      
      public void Search(string directory)
      {
         foreach (var file in Directory.EnumerateFiles(directory))
         {
            var args = new FileArgs(file);
            FileFound?.Invoke(this, args);
            if (args.CancelRequested)
               break;
         }
      }
   }
}