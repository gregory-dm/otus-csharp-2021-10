using System;

namespace Delegates_and_events.FileSearch
{
   public class FileArgs : EventArgs
   {
      public string FoundFile { get; }
      public bool CancelRequested { get; set;}

      public FileArgs(string fileName)
      {
         FoundFile = fileName;
      }
   }
}