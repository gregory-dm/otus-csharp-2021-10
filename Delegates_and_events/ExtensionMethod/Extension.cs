using System;
using System.Collections.Generic;
using System.Linq;

namespace Delegates_and_events.ExtensionMethod
{
   public static class Extension
   {
      public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParameter) where T : class
      {
         if (!e.Any())
            throw new Exception("Collection is empty!");

         T maxObject = null;
         float maxValue = 0f;

         float currentItemValue;

         foreach (var item in e)
         {
            if (maxObject == null)
            {
               maxObject = item;
               maxValue = getParameter(maxObject);
            }

            currentItemValue = getParameter(item);
            if (currentItemValue > maxValue)
            {
               maxObject = item;
               maxValue = currentItemValue;
            }
         }

         return maxObject;
      }
   }
}
