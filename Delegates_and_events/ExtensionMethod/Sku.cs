namespace Delegates_and_events.ExtensionMethod
{
   public class Sku
   {
      public int Id { get; }

      public Sku(int id)
      {
         Id = id;
      }

      public static float GetValue(Sku item)
      {
         return item.Id;
      }
   }
}
