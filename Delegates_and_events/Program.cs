﻿using System;
using System.Collections.Generic;
using Delegates_and_events.FileSearch;
using Delegates_and_events.ExtensionMethod;
using System.Text.Json;

namespace Delegates_and_events
{
   class Program
   {
      static void Main(string[] args)
      {
         var itemList = new List<Sku>
         {
            new Sku(3),
            new Sku(6),
            new Sku(0),
            new Sku(2)
         };

         // Обощённая функция расширения
         var result = itemList.GetMax<Sku>(Sku.GetValue);

         Console.WriteLine(JsonSerializer.Serialize(result));

         string path = "D:\\temp";
         var publisher = new FileSearcher();

         EventHandler<FileArgs> onFileFound = (sender, eventArgs) =>
         {
            Console.WriteLine(eventArgs.FoundFile);
         };

         // Подписка (обработчик с выводом названий файлов каталога)
         publisher.FileFound += onFileFound;

         publisher.Search(path);

         publisher.FileFound -= onFileFound;

         EventHandler<FileArgs> onFirstFileFound = (sender, eventArgs) =>
         {
            Console.WriteLine(eventArgs.FoundFile);
            eventArgs.CancelRequested = true;
         };

         // Подписка (обработчик с выводом названия певого файла и отменой дальнейшего поиска)
         publisher.FileFound += onFirstFileFound;

         publisher.Search(path);
      }
   }
}
