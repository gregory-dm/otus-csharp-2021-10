﻿using eBayDB.Entities;
using eBayDB.Repository;
using System;

namespace eBayDB.Data
{
   public class UserInput
   {
      private readonly IRepository _repository;

      public UserInput(IRepository repository)
      {
         _repository = repository;
      }

      public void Insert(string text)
      {
         string tables = "\n\nSelect table:\n - country\n - users\n - vendors\n - customers";
         while (true)
         {
            Console.WriteLine(text);
            switch (Console.ReadKey().Key)
            {
               case ConsoleKey.Y:
                  SelectTable(tables);
                  break;
               case ConsoleKey.N:
                  Environment.Exit(-1);
                  break;
               default:
                  Console.WriteLine("Invalid input. Try again.");
                  break;
            }
         }
      }

      public void SelectTable(string text)
      {
         Console.WriteLine(text);
         while (true)
         {
            switch (Console.ReadLine().ToString().ToLower())
            {
               case "country":
                  InsertCountry();
                  return;
               case "users":
                  InsertUser();
                  return;
               case "vendors":
                  InsertVendor();
                  return;
               case "customers":
                  InsertCustomer();
                  return;
               default:
                  Console.WriteLine("No such table in database. Try again.");
                  return;
            }
         }
      }

      public void InsertCountry()
      {
         Country country = new Country();

         Console.WriteLine("Name:");
         string name = Console.ReadLine();
         country.Name = name;

         _repository.AddCountry(country);

         Console.WriteLine($"Record inserted successfully: Id: {country.Id} Name: {name}.");
      }

      public void InsertUser()
      {
         User user = new User();

         Console.WriteLine("Login:");
         string login = Console.ReadLine();
         user.Login = login;

         Console.WriteLine("Email:");
         string email = Console.ReadLine();
         user.Email = email;

         Console.WriteLine("ID_Country:");
         int id_country = Int32.Parse(Console.ReadLine());
         user.CountryId = id_country;

         _repository.AddUser(user);

         Console.WriteLine($"Record inserted successfully: Id: {user.Id} Login: {login} Email: {email} ID_Country: {id_country}.");
      }

      public void InsertVendor()
      {
         Vendor vendor = new Vendor();

         Console.WriteLine("ID_User:");
         int id_user = Int32.Parse(Console.ReadLine());
         vendor.UserId = id_user;

         _repository.AddVendor(vendor);

         Console.WriteLine($"Record inserted successfully: Id: {vendor.Id} ID_User: {id_user}.");
      }

      public void InsertCustomer()
      {
         Customer customer = new Customer();

         Console.WriteLine("ID_User:");
         int id_user = Int32.Parse(Console.ReadLine());
         customer.UserId = id_user;

         _repository.AddCustomer(customer);

         Console.WriteLine($"Record inserted successfully: Id: {customer.Id} ID_User: {id_user}.");
      }
   }
}
