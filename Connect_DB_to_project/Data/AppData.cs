﻿using eBayDB.Entities;
using eBayDB.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eBayDB.Data
{
   public class AppData
   {
      private readonly IRepository _repository;

      public AppData(IRepository repository)
      {
         _repository = repository;
      }

      public void AddRecords()
      {
         Country country1 = new Country { Name = "Russia" };
         Country country2 = new Country { Name = "Belarus" };
         Country country3 = new Country { Name = "Ukraine" };
         Country country4 = new Country { Name = "USA" };
         Country country5 = new Country { Name = "Germany" };

         User user1 = new User { Login = "Alex", Email = "alex@gmail.com", Country = country1 };
         User user2 = new User { Login = "Ivan", Email = "ivan@mail.ru", Country = country1 };
         User user3 = new User { Login = "Sergey", Email = "sergey@yandex.ru", Country = country2 };
         User user4 = new User { Login = "Petro", Email = "petr@mail.ru", Country = country3 };
         User user5 = new User { Login = "Nickolas", Email = "nickolas@yahoo.com", Country = country4 };
         User user6 = new User { Login = "Suzanna", Email = "suzy@gmail.com.com", Country = country4 };
         User user7 = new User { Login = "Walter", Email = "walter@aol.com", Country = country5 };

         Vendor vendor1 = new Vendor { User = user1 };
         Vendor vendor2 = new Vendor { User = user3 };
         Vendor vendor3 = new Vendor { User = user5 };
         Vendor vendor4 = new Vendor { User = user6 };
         Vendor vendor5 = new Vendor { User = user7 };

         Customer customer1 = new Customer { User = user2 };
         Customer customer2 = new Customer { User = user3 };
         Customer customer3 = new Customer { User = user4 };
         Customer customer4 = new Customer { User = user6 };
         Customer customer5 = new Customer { User = user7 };

         _repository.AddCountries(new List<Country> { country1, country2, country3, country4, country5 });
         _repository.AddUsers(new List<User> { user1, user2, user3, user4, user5, user6, user7 });
         _repository.AddVendors(new List<Vendor> { vendor1, vendor2, vendor3, vendor4, vendor5 });
         _repository.AddCustomers(new List<Customer> { customer1, customer2, customer3, customer4, customer5 });
      }

      public void Show()
      {
         var countries = _repository.CountriesToList();
         Console.WriteLine("\npublic.country:");
         foreach (Country c in countries)
         {
            Console.WriteLine($"{c.Id} - {c.Name}");
         }

         var users = _repository.UsersToList();
         Console.WriteLine("\npublic.user:");
         foreach (User c in users)
         {
            Console.WriteLine($"{c.Id} - {c.Login} - {c.Email} - {c.CountryId}");
         }

         var vendors = _repository.VendorsToList();
         Console.WriteLine("\npublic.vendors:");
         foreach (Vendor v in vendors)
         {
            Console.WriteLine($"{v.Id} - {v.UserId}");
         }

         var customers = _repository.CustomersToList();
         Console.WriteLine("\npublic.customer:");
         foreach (Customer c in customers)
         {
            Console.WriteLine($"{c.Id} - {c.UserId}");
         }
      }
   }
}
