﻿using eBayDB.Context;
using eBayDB.Entities;
using System.Collections.Generic;
using System.Linq;

namespace eBayDB.Repository
{
   public class AppRepository : IRepository
   {
      private readonly IAppDbContext _appDbContext;

      public AppRepository(IAppDbContext appDbContext)
      {
         _appDbContext = appDbContext;
      }

      public List<Country> CountriesToList()
      {
         return _appDbContext.Country.ToList();
      }

      public List<User> UsersToList()
      {
         return _appDbContext.User.ToList();
      }

      public List<Vendor> VendorsToList()
      {
         return _appDbContext.Vendor.ToList();
      }

      public List<Customer> CustomersToList()
      {
         return _appDbContext.Customer.ToList();
      }

      public void AddCountry(Country country)
      {
         _appDbContext.Country.Add(country);
         _appDbContext.SaveChanges();
      }

      public void AddCountries(IEnumerable<Country> countries)
      {
         _appDbContext.Country.AddRange(countries);
         _appDbContext.SaveChanges();
      }

      public void AddUser(User user)
      {
         _appDbContext.User.Add(user);
         _appDbContext.SaveChanges();
      }

      public void AddUsers(IEnumerable<User> users)
      {
         _appDbContext.User.AddRange(users);
         _appDbContext.SaveChanges();
      }

      public void AddVendor(Vendor vendor)
      {
         _appDbContext.Vendor.Add(vendor);
         _appDbContext.SaveChanges();
      }

      public void AddVendors(IEnumerable<Vendor> vendors)
      {
         _appDbContext.Vendor.AddRange(vendors);
         _appDbContext.SaveChanges();
      }

      public void AddCustomer(Customer customer)
      {
         _appDbContext.Customer.Add(customer);
         _appDbContext.SaveChanges();
      }

      public void AddCustomers(IEnumerable<Customer> customers)
      {
         _appDbContext.Customer.AddRange(customers);
         _appDbContext.SaveChanges();
      }
   }
}
