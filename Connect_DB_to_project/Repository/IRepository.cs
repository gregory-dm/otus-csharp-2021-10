﻿using eBayDB.Entities;
using System.Collections.Generic;

namespace eBayDB.Repository
{
   public interface IRepository
   {
      void AddCountry(Country country);

      void AddCountries(IEnumerable<Country> countries);

      void AddUser(User user);

      void AddUsers(IEnumerable<User> users);

      void AddVendor(Vendor vendor);

      void AddVendors(IEnumerable<Vendor> vendors);

      void AddCustomer(Customer customer);

      void AddCustomers(IEnumerable<Customer> customers);

      List<Country> CountriesToList();

      List<User> UsersToList();

      List<Vendor> VendorsToList();

      List<Customer> CustomersToList();
   }
}
