﻿using eBayDB.Context;
using eBayDB.Data;
using eBayDB.Repository;
using System;

namespace eBayDB
{
   class Program
   {
      static void Main(string[] args)
      {
         try
         {
            IAppDbContext appDbContext = new AppDbContext();
            IRepository repository = new AppRepository(appDbContext);
            AppData data = new AppData(repository);
            data.AddRecords();
            data.Show();
            UserInput userInput = new UserInput(repository);
            userInput.Insert("\nDo you want to insert record into table in eBayDB ? [y/n]");
         }
         catch(Exception ex)
         {
            Console.WriteLine(ex);
         }
      }
   }
}
