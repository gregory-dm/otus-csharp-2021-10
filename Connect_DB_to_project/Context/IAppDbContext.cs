﻿using eBayDB.Entities;
using Microsoft.EntityFrameworkCore;

namespace eBayDB.Context
{
   public interface IAppDbContext
   {
      DbSet<Country> Country { get; set; }
      DbSet<Customer> Customer { get; set; }
      DbSet<User> User { get; set; }
      DbSet<Vendor> Vendor { get; set; }

      void SaveChanges();
   }
}