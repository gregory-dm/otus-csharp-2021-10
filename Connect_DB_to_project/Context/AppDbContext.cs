﻿using eBayDB.Entities;
using Microsoft.EntityFrameworkCore;

namespace eBayDB.Context
{
   public class AppDbContext : DbContext, IAppDbContext
   {
      public DbSet<Country> Country { get; set; }
      public DbSet<User> User { get; set; }
      public DbSet<Vendor> Vendor { get; set; }
      public DbSet<Customer> Customer { get; set; }

      protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
      {
         optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=eBayDB;Username=postgres;Password=***");
      }

      protected override void OnModelCreating(ModelBuilder modelBuilder)
      {
         modelBuilder.Entity<User>()
            .HasOne(b => b.Country)
            .WithMany(i => i.User)
            .HasForeignKey(b => b.CountryId);

         modelBuilder.Entity<Vendor>()
            .HasOne(b => b.User)
            .WithOne(i => i.Vendor)
            .HasForeignKey<Vendor>(b => b.UserId);

         modelBuilder.Entity<Customer>()
            .HasOne(b => b.User)
            .WithOne(i => i.Customer)
            .HasForeignKey<Customer>(b => b.UserId);
      }

      public void SaveChanges()
      {
         base.SaveChanges();
      }
   }
}
