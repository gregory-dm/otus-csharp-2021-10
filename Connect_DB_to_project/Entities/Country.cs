﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eBayDB.Entities
{
   [Table("country")]
   [Index(nameof(Country.Name), IsUnique = true)]
   public class Country
   {
      [Key]
      [Column("id")]
      public int Id { get; set; }

      [Column("name")]
      public string Name { get; set; }

      public List<User> User { get; set; }
   }
}
