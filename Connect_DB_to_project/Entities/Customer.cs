﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eBayDB.Entities
{
   [Table("customers")]
   public class Customer
   {
      [Key]
      [Column("id")]
      public int Id { get; set; }

      [Column("id_user")]
      public int UserId { get; set; }
      public User User { get; set; }
   }
}
