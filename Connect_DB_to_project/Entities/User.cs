﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eBayDB.Entities
{
   [Table("users")]
   [Index(nameof(User.Login), IsUnique = true)]
   [Index(nameof(User.Email), IsUnique = true)]
   public class User
   {
      [Key]
      [Column("id")]
      public int Id { get; set; }

      [Column("login")]
      [MaxLength(255)]
      public string Login { get; set; }

      [Column("email")]
      [MaxLength(255)]
      public string Email { get; set; }

      [Column("id_country")]
      public int CountryId { get; set; }
      public Country Country { get; set; }

      public Customer Customer { get; set; }

      public Vendor Vendor { get; set; }
   }
}
