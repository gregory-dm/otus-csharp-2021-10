﻿
namespace Reflection
{
   public class F : ITestClass
   {
      [CsvColumn(Index = 0)]
      public int i1 { get; set; }

      [CsvColumn(Index = 1)]
      public int i2 { get; set; }

      [CsvColumn(Index = 2)]
      public int i3 { get; set; }

      [CsvColumn(Index = 3)]
      public int i4 { get; set; }

      [CsvColumn(Index = 4)]
      public int i5 { get; set; }

      public F Get()
      {
         return new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
      }
   }
}
