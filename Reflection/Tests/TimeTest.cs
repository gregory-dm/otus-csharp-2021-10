﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;

namespace Reflection
{
   public class TimeTest
   {
      private ICsvSerializer _csvSerialiser;
      private ITestClass _testClass;
      private Stopwatch timer;
      private int iterations = 1000;

      public TimeTest(ICsvSerializer csvSerializer, ITestClass testClass)
      {
         _csvSerialiser = csvSerializer;
         _testClass = testClass;
      }
      public void ExecuteCsvSerializationTest()
      {
         timer = new Stopwatch();
         string lines = "";

         int count = 0;
         timer.Start();
         while (count < iterations)
         {
            lines = _csvSerialiser.Serialize(_testClass.Get());
            count++;
         }
         timer.Stop();
         Console.WriteLine("CsvSerializer serialization result: {0} time(ms): {1}", lines, timer.ElapsedMilliseconds);
      }

      public void ExecuteCsvDeserializationTest()
      {
         timer = new Stopwatch();
         string lines = _csvSerialiser.Serialize(_testClass.Get());

         int count = 0;
         timer.Start();
         while (count < iterations)
         {
            _testClass = _csvSerialiser.Deserialize<F>(lines);
            count++;
         }
         timer.Stop();
         Console.WriteLine("CsvSerializer deserialization time(ms): " + timer.ElapsedMilliseconds);
      }

      public void ExecuteJsonSerializationTest()
      {
         timer = new Stopwatch();
         string lines = "";

         int count = 0;
         timer.Start();
         while (count < iterations)
         {
            lines = JsonConvert.SerializeObject(_testClass.Get());
            count++;
         }
         timer.Stop();
         Console.WriteLine("NewtonsoftJson serialization result: {0} time(ms): {1}", lines, timer.ElapsedMilliseconds);
      }

      public void ExecuteJsonDeserializationTest()
      {
         timer = new Stopwatch();
         string lines = JsonConvert.SerializeObject(_testClass.Get());

         int count = 0;
         timer.Start();
         while (count < iterations)
         {
            _testClass = JsonConvert.DeserializeObject<F>(lines);
            count++;
         }
         timer.Stop();
         Console.WriteLine("NewtonsoftJson deserialization time(ms): " + timer.ElapsedMilliseconds);
      }

      public void ExecuteTests()
      {
         var tests = new TimeTest(_csvSerialiser, _testClass);
         tests.ExecuteCsvSerializationTest();
         tests.ExecuteCsvDeserializationTest();
         tests.ExecuteJsonSerializationTest();
         tests.ExecuteJsonDeserializationTest();
      }
   }
}
