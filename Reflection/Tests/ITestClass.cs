﻿namespace Reflection
{
   public interface ITestClass
   {
      F Get();
   }
}