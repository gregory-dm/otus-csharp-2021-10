﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace Reflection
{
   public class CsvSerializer : ICsvSerializer
   {
      private const string separator = ",";
      public string Serialize(object item)
      {
         var values = GetOrderedSerializableProperties(item.GetType())
               .Select(p => p.GetValue(item));

         return string.Join(separator, values);
      }

      public T Deserialize<T>(string line) where T : new()
      {
         var properties = GetOrderedSerializableProperties(typeof(T)).ToArray();
         var values = line.Split(separator);
         T item = new T();

         for (int i = 0; i < values.Length; i++)
         {
            var property = properties[i];
            var propertyType = property.PropertyType;
            bool isConvertible = propertyType
               .GetInterfaces()
               .Any(i => i == typeof(IConvertible));
            object targetValue = values[i];

            if (isConvertible)
               targetValue = (values[i] as IConvertible).ToType(propertyType, CultureInfo.InvariantCulture);
            else
               throw new NotSupportedException("Property should implement IConvertible");

            properties[i].SetValue(item, targetValue);
         }

         return item;
      }

      private IEnumerable<PropertyInfo> GetOrderedSerializableProperties(Type type)
      {
         var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
         var serializableProperties = properties
            .Where(p => p.GetCustomAttributes().Any(a => a.GetType() == typeof(CsvColumnAttribute)));
         var orderedSerializableProperties = serializableProperties
            .OrderBy(p => p.GetCustomAttribute<CsvColumnAttribute>().Index);

         return orderedSerializableProperties;
      }


   }
}
