﻿namespace Reflection
{
   public interface ICsvSerializer
   {
      T Deserialize<T>(string line) where T : new();
      string Serialize(object item);
   }
}