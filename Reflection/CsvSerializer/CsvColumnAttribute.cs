﻿using System;

namespace Reflection
{
   [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
   sealed class CsvColumnAttribute : Attribute
   {
      public int Index { get; set; }
   }
}
