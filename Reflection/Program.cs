﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Reflection
{
   class Program
   {
      static void Main(string[] args)
      {
         var host = Host.CreateDefaultBuilder()
            .ConfigureServices((context, services) =>
            {
               services.AddTransient<ICsvSerializer, CsvSerializer>();
               services.AddTransient<ITestClass, F>();
            })
            .Build();

         var service = ActivatorUtilities.CreateInstance<TimeTest>(host.Services);
         service.ExecuteTests();
      }
   }
}
