using System;

namespace Prototype
{
    public class RetailChainClient : WholesaleClient, ICloneable
    {
        public int Discount;
        public RetailChainClient(int Id, string Name, int discount) : base(Id, Name)
        {
            Discount = discount;
        }
        public override RetailChainClient MyClone()
        {
            return new RetailChainClient(Id, Name, Discount);
        }
        public override object Clone()
        {
            return MyClone();
        }
    }
}