using System;

namespace Prototype
{
    public class WholesaleClient : Client, ICloneable
    {
        public string Name;
        public WholesaleClient(int Id, string name) : base(Id)
        {
            Name = name;
        }
        public override WholesaleClient MyClone()
        {
            return new WholesaleClient(Id, Name);
        }
        public override object Clone()
        {
            return MyClone();
        }
    }
}