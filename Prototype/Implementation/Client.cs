using System;

namespace Prototype
{
    public class Client : IMyCloneable<Client>, ICloneable
    {
        public int Id;
        public Client(int id)
        {
            Id = id;
        }
        public virtual Client MyClone()
        {
            return new Client(Id);
        }
        public virtual object Clone()
        {
            return MyClone();
        }
    }
}