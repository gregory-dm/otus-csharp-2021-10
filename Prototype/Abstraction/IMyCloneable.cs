namespace Prototype
{
    interface IMyCloneable<T>
    {
        T MyClone();
    }
}