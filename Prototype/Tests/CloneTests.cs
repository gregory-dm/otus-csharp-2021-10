using Xunit;

namespace Prototype.Tests
{
    public class CloneTests
    {
        [Fact]
        public void MyCopyClientGetDifferentObjectsWithSameId()
        {
            // Arrange
            var id = 1;
            var client = new Client(id);
            // Act
            var clientClone = client.MyClone();
            // Assert
            Assert.Equal(client.Id, clientClone.Id);
            Assert.NotSame(client, clientClone);
        }
        [Fact]
        public void MyCopyWholesaleClientGetDifferentObjectsWithSameIdName()
        {
            // Arrange
            var id = 1;
            var name = "wholesale";
            var wholesaleClient = new WholesaleClient(id, name);
            // Act
            var wholesaleClientClone = wholesaleClient.MyClone();
            // Assert
            Assert.Equal(wholesaleClient.Id, wholesaleClientClone.Id);
            Assert.Equal(wholesaleClient.Name, wholesaleClientClone.Name);
            Assert.NotSame(wholesaleClient, wholesaleClientClone);
        }
        [Fact]
        public void MyCopyRetailChainClientGetDifferentObjectsWithSameIdNameDiscount()
        {
            // Arrange
            var id = 1;
            var name = "retail";
            var discount = 10;
            var retailChainClient = new RetailChainClient(id, name, discount);
            // Act
            var retailChainClientClone = retailChainClient.MyClone();
            // Assert
            Assert.Equal(retailChainClient.Id, retailChainClientClone.Id);
            Assert.Equal(retailChainClient.Name, retailChainClientClone.Name);
            Assert.Equal(retailChainClient.Discount, retailChainClientClone.Discount);
            Assert.NotSame(retailChainClient, retailChainClientClone);
        }
        [Fact]
        public void CopyClientGetDifferentObjectsWithSameId()
        {
            // Arrange
            var id = 1;
            var client = new Client(id);
            // Act
            var clientClone = (Client)client.Clone();
            // Assert
            Assert.Equal(client.Id, clientClone.Id);
            Assert.NotSame(client, clientClone);
        }
        [Fact]
        public void CopyWholesaleClientGetDifferentObjectsWithSameIdName()
        {
            // Arrange
            var id = 1;
            var name = "wholesale";
            var wholesaleClient = new WholesaleClient(id, name);
            // Act
            var wholesaleClientClone = (WholesaleClient)wholesaleClient.Clone();
            // Assert
            Assert.Equal(wholesaleClient.Id, wholesaleClientClone.Id);
            Assert.Equal(wholesaleClient.Name, wholesaleClientClone.Name);
            Assert.NotSame(wholesaleClient, wholesaleClientClone);
        }
        [Fact]
        public void CopyRetailChainClientGetDifferentObjectsWithSameIdNameDiscount()
        {
            // Arrange
            var id = 1;
            var name = "retail";
            var discount = 10;
            var retailChainClient = new RetailChainClient(id, name, discount);
            // Act
            var retailChainClientClone = (RetailChainClient)retailChainClient.Clone();
            // Assert
            Assert.Equal(retailChainClient.Id, retailChainClientClone.Id);
            Assert.Equal(retailChainClient.Name, retailChainClientClone.Name);
            Assert.Equal(retailChainClient.Discount, retailChainClientClone.Discount);
            Assert.NotSame(retailChainClient, retailChainClientClone);
        }
    }
}