﻿using System.Collections.Generic;
using System.IO;
using Otus.Teaching.Concurrency.Import.Core.AppSettings;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser : IDataParser<List<Customer>>
    {
        ISettings _settings;

        public XmlParser(ISettings settings)
        {
            _settings = settings;
        }

        public List<Customer> Parse()
        {
            CustomersList customers = new CustomersList();
            using (FileStream stream = new FileStream(_settings.DataFileName, FileMode.Open, FileAccess.Read))
            using (StreamReader reader = new StreamReader(stream))
            {
                customers = (CustomersList)new XmlSerializer(typeof(CustomersList)).Deserialize(stream);
            }
            return customers.Customers;
        }
    }
}