using System;
using System.Collections.Generic;
using System.IO;
using Otus.Teaching.Concurrency.Import.Core.AppSettings;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser : IDataParser<List<Customer>>
    {
        ISettings _settings;

        public CsvParser(ISettings settings)
        {
            _settings = settings;
        }

        public List<Customer> Parse()
        {
            var customers = new List<Customer>();
            using (FileStream stream = new FileStream(_settings.DataFileName, FileMode.Open, FileAccess.Read))
            using (StreamReader reader = new StreamReader(stream))
            {
                while(!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(",");

                    customers.Add(
                        new Customer()
                        { 
                            Id = Int32.Parse(values[0]),
                            FullName = values[1],
                            Email = values[2],
                            Phone = values[3]
                        }
                    );
                }
            }
            return customers;
        }
    }
}