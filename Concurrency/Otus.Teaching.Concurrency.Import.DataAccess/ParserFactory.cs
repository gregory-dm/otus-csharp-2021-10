using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Core.AppSettings;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using CsvDataParser = Otus.Teaching.Concurrency.Import.DataAccess.Parsers.CsvParser;
using XmlDataParser = Otus.Teaching.Concurrency.Import.DataAccess.Parsers.XmlParser;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public static class ParserFactory
    {
        public static IDataParser<List<Customer>> GetParser(ISettings settings)
        {
            switch (settings.DataFileFormat)
            {
                case "csv":
                    return new CsvDataParser(settings);
                case "xml":
                default:
                    return new XmlDataParser(settings);
            }
        }
    }
}