using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.AppSettings;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class DataContext : DbContext
    {
        private readonly string _connectionString;

        public DataContext(ISettings settings)
        {
            _connectionString = settings.ConnectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
        }

        public DbSet<Customer> Customers { get; set; }
    }
}