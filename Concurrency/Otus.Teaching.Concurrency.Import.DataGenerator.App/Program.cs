﻿using System;
using System.IO;
using Otus.Teaching.Concurrency.Import.Core.AppSettings;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    class Program
    {
        private static string _dataFileName;
        private static string _dataFileFormat;
        private static int _dataCount = 100; 
        
        static void Main(string[] args)
        {
            Settings settings = new Settings();
                        
            if (!TryValidateAndParseArgs(args))
                return;

            settings.DataFileName = _dataFileName;
            settings.DataFileFormat = _dataFileFormat;
            settings.DataCount = _dataCount;
            
            Console.WriteLine($"Generating {_dataFileFormat} data...");

            var generator = GeneratorFactory.GetGenerator(settings);
            
            generator.Generate();
            
            Console.WriteLine($"Generated data in {_dataFileName}.{_dataFileFormat}...");
        }

        private static bool TryValidateAndParseArgs(string[] args)
        {
            if (args != null && args.Length > 0)
            {
                _dataFileName = args[0];
            }
            else
            {
                Console.WriteLine("Data file name is required");
                return false;
            }

            if (args.Length > 1)
            {
                _dataFileFormat = args[1];
            }
            else
            {
                Console.WriteLine("Data file extension is required");
                return false;
            }
            
            if (args.Length > 2)
            {
                if (!int.TryParse(args[2], out _dataCount))
                {
                    Console.WriteLine("Data must be integer");
                    return false;
                }
            }
            return true;
        }
    }
}