using Otus.Teaching.Concurrency.Import.Core.AppSettings;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;
using CsvDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.CsvGenerator;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    public static class GeneratorFactory
    {
        public static IDataGenerator GetGenerator(ISettings settings)
        {
            switch(settings.DataFileFormat)
            {
                case "csv":
                    return new CsvDataGenerator(settings);
                case "xml":
                default:
                    return new XmlDataGenerator(settings);
            }
        }
    }
}