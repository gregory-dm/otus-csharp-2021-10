using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.Core.AppSettings;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.XmlGenerator;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class ConcurrencyTest
    {
        private static List<Customer> _customers;
        private static readonly string _dataFileDirectory = AppDomain.CurrentDomain.BaseDirectory;
        private readonly ISettings _settings;

        public ConcurrencyTest(IConfiguration config)
        {
            _settings = config.GetSection("Settings").Get<Settings>();
        }

        public void Start()
        {
            _settings.DataFileName = Path.Combine(_dataFileDirectory, string.Concat(_settings.DataFileName, ".", _settings.DataFileFormat));

            Console.WriteLine($"Generator started with process Id {Process.GetCurrentProcess().Id}...");

            Stopwatch stopwatch = new Stopwatch();
            ProcessStarter process = new ProcessStarter(_settings);
            IDataParser<List<Customer>> dataParser = ParserFactory.GetParser(_settings);

            switch (_settings.StartupType)
            {
                case "process":
                    Console.WriteLine("Start process");
                    stopwatch.Start();
                    process.StartProcess();
                    stopwatch.Stop();
                    break;
                default:
                case "method":
                    Console.WriteLine("Start method");
                    stopwatch.Start();
                    GeneratorFactory.GetGenerator(_settings).Generate();
                    stopwatch.Stop();
                    break;
            };
            Console.WriteLine($"Generation time: {stopwatch.Elapsed.TotalSeconds} sec.");

            stopwatch.Restart();
            _customers = dataParser.Parse();
            stopwatch.Stop();
            Console.WriteLine($"Parsing time: {stopwatch.Elapsed.TotalSeconds} sec.");

            FakeDataLoader fakeDataLoader = new FakeDataLoader(_customers, _settings);

            stopwatch.Restart();
            fakeDataLoader.LoadData();
            stopwatch.Stop();
            Console.WriteLine($"Load to DB time: {stopwatch.Elapsed.TotalSeconds} sec.");
        }
    }
}