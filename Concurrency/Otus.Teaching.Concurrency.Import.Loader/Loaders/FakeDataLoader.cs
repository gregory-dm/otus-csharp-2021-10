using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Core.AppSettings;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class FakeDataLoader : IDataLoader
    {
        private List<Customer> _customers;
        ISettings _settings;

        public FakeDataLoader(List<Customer> customers, ISettings settings)
        {
            _customers = customers;
            _settings = settings;
        }
        public void LoadData()
        {
            int pageSize = (_settings.DataCount/_settings.Threads) + 1;
            int threads = _settings.Threads;
            int countTries = _settings.Tries;

            using (var countdownEvent = new CountdownEvent(threads))
            {
                IEnumerable<Customer[]> chunks = _customers.Chunk(pageSize);
                foreach (var item in chunks)
                {
                    List<Customer> list = item.ToList();
                    ThreadPool.QueueUserWorkItem(
                        x =>
                        {
                            int count = 0;
                            while(count < countTries)
                            {
                                try
                                {   
                                    var customers = list;
                                    using (DataContext context = new DataContext(_settings))
                                    {
                                        CustomerRepository repository = new CustomerRepository(context);
                                        foreach (var customer in customers)
                                            repository.AddCustomer(customer);

                                        context.SaveChanges();
                                    }
                                    countdownEvent.Signal();
                                    count = countTries;
                                    Console.WriteLine("InitialCount={0}, CurrentCount={1}, IsSet={2}", countdownEvent.InitialCount, countdownEvent.CurrentCount, countdownEvent.IsSet);
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                    count++;
                                }
                            }
                        });
                    }
                countdownEvent.Wait();
            }
            Console.WriteLine("Done loading.");
        }
    }
}