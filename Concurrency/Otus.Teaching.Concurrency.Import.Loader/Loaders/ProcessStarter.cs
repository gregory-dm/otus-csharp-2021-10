using System.Diagnostics;
using System.IO;
using Otus.Teaching.Concurrency.Import.Core.AppSettings;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class ProcessStarter
    {
        ISettings _settings;

        public ProcessStarter(ISettings settings)
        {
            _settings = settings;
        }
        public void StartProcess()
        {
            ProcessStartInfo startInfo = new ProcessStartInfo()
            {
                ArgumentList = { _settings.DataFileName, _settings.DataFileFormat, _settings.DataCount.ToString() },
                FileName = Path.Combine(_settings.ProcessPath, _settings.ProcessFileName)
            };

            Process process = Process.Start(startInfo);
            process.WaitForExit();
        }
    }
}