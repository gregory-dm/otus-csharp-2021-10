using System.IO;
using Otus.Teaching.Concurrency.Import.Core.AppSettings;
using Otus.Teaching.Concurrency.Import.Handler.Data;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CsvGenerator : IDataGenerator
    {
        ISettings _settings;

        public CsvGenerator(ISettings settings)
        {
            _settings = settings;
        }

        public void Generate()
        {
            var customerList = RandomCustomerGenerator.Generate(_settings.DataCount);

            using (FileStream stream = new FileStream(_settings.DataFileName, FileMode.Create, FileAccess.Write))
            using (StreamWriter writer = new StreamWriter(stream))
            {
                foreach (var customer in customerList)
                    writer.WriteLine($"{customer.Id},{customer.FullName},{customer.Email},{customer.Phone}");
            };
        }
    }
}