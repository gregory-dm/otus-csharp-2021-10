﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Data
{
   public interface ICustomerRepository
   {
      IEnumerable<Customer> GetAllCustomers();

      Task<Customer> GetCustomerByIdAsync(long id);

      void AddCustomer(Customer customer);

      Task SaveChangesAsync();
   }
}
