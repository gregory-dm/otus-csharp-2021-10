﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Data
{
   public class CustomerContext : DbContext
   {
      public CustomerContext(DbContextOptions<CustomerContext> opt) : base(opt) { }

      public DbSet<Customer> Customers { get; set; }
   }
}
