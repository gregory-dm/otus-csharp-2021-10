﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Data
{
   public class SqlCustomerRepository : ICustomerRepository
   {
      private readonly CustomerContext _context;

      public SqlCustomerRepository(CustomerContext context)
      {
         _context = context;
      }

      public void AddCustomer(Customer customer)
      {
         if (customer == null)
            throw new ArgumentNullException(nameof(customer));

         _context.Customers.Add(customer);
      }

      public IEnumerable<Customer> GetAllCustomers()
      {
         return _context.Customers;
      }

      public async Task<Customer> GetCustomerByIdAsync(long id)
      {
         return await _context.Customers.FindAsync(id);
      }

      public async Task SaveChangesAsync()
      {
         await _context.SaveChangesAsync();
      }
   }
}
