﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace WebApi.Dtos
{
   public class ConsoleDto
   {
      public HttpStatusCode StatusCode { get; set; }

      public long Id { get; set; }
   }
}
