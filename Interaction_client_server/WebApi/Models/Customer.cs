using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApi.Models
{
   public class Customer
   {
      [Column("id")]
      public long Id { get; set; }
      
      [Required]
      [Column("firstname")]
      public string Firstname { get; set; }

      [Required]
      [Column("lastname")]
      public string Lastname { get; set; }
   }
}