﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Dtos;
using WebApi.Models;

namespace WebApi.Profiles
{
   public class CustomersProfile : Profile
   {
      public CustomersProfile()
      {
         CreateMap<Customer, CustomerReadDto>();
         CreateMap<CustomerAddDto, Customer>();
         CreateMap<ObjectResult, ConsoleDto>()
            .ForMember(dest => dest.StatusCode, opt => opt.MapFrom(src => src.StatusCode))
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Value != null ? (long)src.Value : default));
      }
   }
}
