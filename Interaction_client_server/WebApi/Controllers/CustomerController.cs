using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebApi.Data;
using WebApi.Dtos;
using WebApi.Models;

namespace WebApi.Controllers
{
   [Route("api/customers")]
   [ApiController]
   public class CustomerController : ControllerBase
   {
      private readonly ICustomerRepository _repository;
      private readonly IMapper _mapper;

      public CustomerController(ICustomerRepository repository, IMapper mapper)
      {
         _repository = repository;
         _mapper = mapper;
      }

      [HttpGet("{id}")]
      public async Task<ActionResult<CustomerReadDto>> GetCustomerById(long id)
      {
         var customerItem = await _repository.GetCustomerByIdAsync(id);

         if (customerItem == null)
            return NotFound();

         return Ok(_mapper.Map<CustomerReadDto>(customerItem));
      }

      [HttpGet]
      public ActionResult <IEnumerable<CustomerReadDto>> GetAllCustomers()
      {
         var customerItems = _repository.GetAllCustomers();
         return Ok(_mapper.Map<IEnumerable<CustomerReadDto>>(customerItems));
      }

      [HttpPost]
      public async Task<ActionResult<ConsoleDto>> AddCustomer(CustomerAddDto customer)
      {
         var customerItem = await _repository.GetCustomerByIdAsync(customer.Id);

         if (customerItem != null)
            return Conflict(customer.Id);

         var customerModel = _mapper.Map<Customer>(customer);
         _repository.AddCustomer(customerModel);
         await _repository.SaveChangesAsync();

         var dto = _mapper.Map<ConsoleDto>(Ok(customerModel.Id));

         return Ok(dto);
      }
   }
}