﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebClient
{
   public class UserInput
   {
      HttpRequest httpRequest = new();
      CustomerCreateRequest customerCreateRequest = new();
      public async Task PromptAsync()
      {
         while (true)
         {
            Console.WriteLine($"Options:\n 1. Request customer\n 2. Add random customer\n 3. Exit application\n\nEnter option [1/2/3]:");
            switch (Console.ReadLine())
            {
               case "1":
                  await RequestPrompt();
                  break;
               case "2":
                  await AddPrompt();
                  break;
               case "3":
                  Environment.Exit(-1);
                  break;
               default:
                  Console.WriteLine("Invalid input. Try again.");
                  break;
            }
         }
      }

      public async Task RequestPrompt()
      {
         long id;
         while (true)
         {
            Console.WriteLine("Enter customer id:");
            if (Int64.TryParse(Console.ReadLine(), out id))
            {
               var customer = await httpRequest.GetCustomerByIdAsync(id);
               Console.WriteLine(customer);
               break;
            }
            else
               Console.WriteLine("Invalid input. Try again.");
         }
      }

      public async Task AddPrompt()
      {
         var res = await httpRequest.SendRequestAsync(customerCreateRequest.RandomCustomer());
         var customer = await httpRequest.GetCustomerByIdAsync(res.Id);
         Console.WriteLine(customer);
      }
   }
}
