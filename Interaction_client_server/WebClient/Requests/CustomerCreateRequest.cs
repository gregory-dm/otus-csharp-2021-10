using System;

namespace WebClient
{
   public class CustomerCreateRequest
   {
      public CustomerCreateRequest()
      {
      }

      public CustomerCreateRequest(
          string firstName,
          string lastName)
      {
          Firstname = firstName;
          Lastname = lastName;
      }

      public string Firstname { get; set; }

      public string Lastname { get; set; }

      public  CustomerCreateRequest RandomCustomer()
      {
         var names = new string[]
         {
            "Noah" ,"Liam" ,"William" ,"Mason" ,
            "James" ,"Benjamin" ,"Jacob" ,
            "Michael" ,"Elijah" ,"Ethan" ,
            "Alexander" ,"Oliver" ,"Daniel" ,
            "Lucas" ,"Matthew" ,"Aiden" ,
            "Jackson" ,"Logan" ,"David" ,"Joseph" ,
            "Samuel" ,"Henry" ,"Owen" ,"Sebastian" ,
            "Gabriel" ,"Carter" ,"Jayden" ,"John" ,
            "Luke" ,"Anthony"
         };

         var r = new Random();
         var indexFirstName = r.Next(names.Length);
         var indexLastName = r.Next(names.Length);

         return new CustomerCreateRequest(names[indexFirstName], names[indexLastName]);
      }
   }
}