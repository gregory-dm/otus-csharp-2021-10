﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebClient
{
   public class HttpRequest
   {
      private readonly HttpClient client = new HttpClient { BaseAddress = new Uri("https://localhost:5001/") };

      public async Task<ConsoleDto> SendRequestAsync(CustomerCreateRequest customerCreateRequest)
      {
         var response = await client.PostAsJsonAsync("/api/customers", customerCreateRequest);
         response.EnsureSuccessStatusCode();
         var result = await response.Content.ReadAsAsync<ConsoleDto>();
         return result;
      }

      public async Task<Customer> GetCustomerByIdAsync(long id)
      {
         var response = await client.GetAsync($"/api/customers/{id}");
         response.EnsureSuccessStatusCode();
         var result = await response.Content.ReadAsAsync<Customer>();
         return result;
      }
   }
}
