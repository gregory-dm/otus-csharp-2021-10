﻿using System.Net;

namespace WebClient
{
   public class ConsoleDto
   {
      public HttpStatusCode StatusCode { get; set; }

      public long Id { get; set; }
   }
}
