﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;

namespace WebClient
{
   static class Program
   {
      static async Task Main(string[] args)
      {
         try
         {
            UserInput userInput = new();
            await userInput.PromptAsync();
         }
         catch (Exception ex)
         {
            Console.WriteLine(ex);
         }
         finally
         {
            Console.ReadLine();
         }
      }
   }
}